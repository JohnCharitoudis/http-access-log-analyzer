[![pipeline status](https://gitlab.com/JohnCharitoudis/http-access-log-analyzer/badges/master/build.svg)](https://gitlab.com/JohnCharitoudis/http-access-log-analyzer)

# Description

Simple HTTP access log analysis program written in Python 2 for post processing [NASA's HTTP server access logs from August '95](http://ita.ee.lbl.gov/html/contrib/NASA-HTTP.html).

The program generates a report for the following:
- The top 10 requested pages and the number of requests made for each.
- Percentage of successful requests (status code within the range of 200 and 300).
- Percentage of unsuccessful requests (status code out of range of 200 and 300).
- Top 10 unsuccessful page requests.
- The top 10 hosts making the most requests, report contains the IP address and number of requests made.
- For each of the top 10 hosts, shows the top 5 pages requested and the number of requests for each.


# Requirements

Only Python 2 along with `jinja2` library are required for the program to run. No need to install `jinja2` on your own, the program will take care if not present in the system.

# Assumptions

1. Log lines that don't comply with the format described [here](http://ita.ee.lbl.gov/html/contrib/NASA-HTTP.html) are considered malformed.
2. In case of malformed line the program raises an error indicating the line number and the actual line and continues.
3. All report results have been calculated from the access log lines that were NOT considered as malformed.

# Usage

To use the program simply run:

```python
python core.py
```

The program also supports command line arguments. To check all the supported option run:

```python
python core.py --help
```
To run the Unit Tests run:
```python
python -m unittest discover -s tests -p 'test_*.py'
```