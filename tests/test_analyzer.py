'''
Created on Nov 11, 2017

@author: johncharitoudis
'''
import unittest, os
from http_access_log_analyzer import core

unittest.TestLoader.sortTestMethodsUsing=None

class TestAnalyzerMethods(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestAnalyzerMethods, cls).setUpClass()
        cls.analyzer = core.Analyzer()


    def test_access_logs_downloading(self):
        self.analyzer.download_access_logs()
        self.analyzer.unzip_access_logs()
        self.assertTrue(os.path.exists(self.analyzer.artifacts + self.analyzer.log_archive),
                        'Failed to download access log archive.')
        self.assertTrue(os.path.exists(self.analyzer.artifacts + self.analyzer.log_file),
                        'Failed to uncompress access log archive.')


    def test_data_initiation(self):
        self.analyzer.initiate_data()
        self.assertNotEqual(self.analyzer.data['HOST'], [], 'Failed to initiate the data.')
        self.assertNotEqual(self.analyzer.data['TIMESTAMP'], [], 'Failed to initiate the data.')
        self.assertNotEqual(self.analyzer.data['REQUEST'], [], 'Failed to initiate the data.')
        self.assertNotEqual(self.analyzer.data['STATUS_CODE'], [], 'Failed to initiate the data.')
        self.assertNotEqual(self.analyzer.data['BYTES'], [], 'Failed to initiate the data.')

    def test_data_post_processing(self):
        self.assertNotEqual(self.analyzer.calculate_top_requested_pages(), {}, 'Failed to calculate top requested pages.')
        self.assertNotEqual(self.analyzer.calculate_top_unsuccessful_requested_pages(), {}, 'Failed to calculate top unsuccessful pages.')
        self.assertNotEqual(self.analyzer.calculate_top_hosts(), {}, 'Failed to calculate top requested hosts.')
        self.assertNotEqual(self.analyzer.calculate_top_hosts_and_top_requested_pages(), {}, 'Failed to calculate top requested hosts and requested pages.')
        self.assertNotEqual(self.analyzer.calculate_percentage_of_successful_requests(), (), 'Failed to calculate percentage of successful requests.')
        self.assertNotEqual(self.analyzer.calculate_percentage_of_unsuccessful_requests(), (), 'Failed to calculate percentage of unsuccessful requests.')

#    def test_report_generation(self):
#        self.analyzer.generate_report(template_data)
#        self.assertTrue(os.path.exists(self.analyzer.artifacts + self.analyzer.report_file),
#                        'Failed to create the report file.')

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()