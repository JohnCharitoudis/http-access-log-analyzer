'''
Created on Nov 11, 2017

@author: johncharitoudis
'''
import os, sys, urllib, gzip, re, collections
try:
    __import__('jinja2')
except ImportError:
    import pip
    pip.main(['install', 'jinja2'])
finally:
    from jinja2     import Environment, FileSystemLoader
from argparse   import ArgumentParser
from errors     import *

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
FTP_URL = 'ftp://ita.ee.lbl.gov/traces/NASA_access_log_Aug95.gz'
TEMPLATE = 'Access_Log_Report.html.j2'

class Analyzer(object):

    '''
    Simple program for analyzing formatted HTTP access logs.
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.data         = {'HOST':[],
                             'TIMESTAMP':[],
                             'REQUEST':[],
                             'STATUS_CODE':[],
                             'BYTES':[]}
        self.log_archive  = FTP_URL.split('/')[-1]
        self.log_file     = self.log_archive.split('.', 1)[0]
        self.artifacts    = 'artifacts/'
        self.report_file  = 'Access_Log_Report'
        
        #self._cleanup()

    def download_access_logs(self):
        '''
        Downloads the HTTP access log archive from NASA's FTP Server.
        
        Args:

        Returns:
        
        '''
        if os.path.exists(self.artifacts + self.log_archive):
            print('Access log archive already downloaded...')
            return
        try:
            print("Downloading access log archive...")
            urllib.urlretrieve(FTP_URL, self.artifacts + self.log_archive)
        except Exception:
            msg = "Error downloading file '{}'. Check your proxy settings."
            raise DownloadError(msg.format(self.log_archive))

    def unzip_access_logs(self):
        '''
        Uncompresses the HTTP access log archive.
        
        Args:

        Returns:
        
        '''
        if os.path.exists(self.artifacts + self.log_file):
            print('Access log file already exists...')
            return
        try:
            print("Uncompressing access log archive...")
            with gzip.open(self.artifacts + self.log_archive, 'rb') as g:
                with open(self.artifacts + self.log_file, 'wb') as f:
                    f.write(g.read())
        except IOError:
            msg = "Failed to uncompress '{}' ."
            raise UnzipError(msg.format(self.log_archive))

    def initiate_data(self):
        '''
        Parses the HTTP access logs and store them to memory in dictionary format. 
        
        Args:

        Returns:
        
        '''
        gen = self._read_data()
        for item in gen:
            self.data['HOST'].append(item[0])
            self.data['TIMESTAMP'].append(item[1])
            self.data['REQUEST'].append(item[2])
            self.data['STATUS_CODE'].append(int(item[3]))
            self.data['BYTES'].append(item[4])

    def generate_report(self, template_data):
        '''
        Generates a report that may contain the following:
        - The top 10 requested pages and the number of requests made for each.
        - Percentage of successful requests (status code within the range of 200 and 300).
        - Percentage of unsuccessful requests (status code out of range of 200 and 300).
        - Top 10 unsuccessful page requests.
        - The top 10 hosts making the most requests, report contains the IP address and number of requests made.
        - For each of the top 10 hosts, shows the top 5 pages requested and the number of requests for each.
        
        Args:
            template_data: A dictionary containing all the data that will
                           be render to the template

        Returns:
        
        '''
        report = os.path.splitext(TEMPLATE)[0]
        j2_env = Environment(loader = FileSystemLoader(CURRENT_DIR),
                             trim_blocks = True)
        with open(self.artifacts + report, 'wb') as f:
            try:
                f.write(j2_env.get_template(TEMPLATE).render(template_data))
            except Exception as e:
                print e
                msg = 'Failed to create report {}'
                raise ReportError(msg.format(report))
        print('Report {} successfully created!'.format(report))

    def calculate_top_requested_pages(self):
        '''
        Calculates the top 10 requested pages and the number of requests
        made for each.
        
        Args:

        Returns:
            A dictionary with keys the top requested pages and values the
            number of requests.
        
        '''
        dic = {}
        for count in self.get_top_requested_pages().most_common(10):
            dic[count[0]] = count[1]
        return dic

    def calculate_top_unsuccessful_requested_pages(self):
        '''
        Calculates the top 10 unsuccessful page requests.
        
        Args:

        Returns:
            A dictionary with keys the top unsuccessfully requested pages
            and values the number of requests.
        
        '''
        dic = {}
        for count in self.get_top_unsuccessful_requested_pages().most_common(10):
            dic[count[0]] = count[1]
            
        return dic
    
    def calculate_top_hosts(self):
        '''
        Calculates the top 10 hosts making the most requests. Report 
        contains the IP address or domain name and the number of requests made.
        
        Args:

        Returns:
            A dictionary with keys the top requested hosts and values the
            number of requests.
        
        '''
        dic = {}
        for count in self.get_top_hosts_that_request_pages().most_common(10):
            dic[count[0]] = count[1]
        
        return dic

    def calculate_top_hosts_and_top_requested_pages(self):
        '''
        Calculates the top 10 hosts making the most requests. For each of
        the top 10 hosts, shows the top 5 pages requested and the number 
        of requests for each
        
        Args:

        Returns:
            A dictionary with keys the top requested hosts and values a 
            nested dictionary with keys the top requested pages and values
            the number of requests.
        
        '''
        dic = {}
        top_hosts = self.get_top_hosts_that_requested_pages_and_the_top_pages()
        for host in top_hosts:
            c = {}
            for count in collections.Counter(top_hosts[host]).most_common(5):
                c[count[0]] = count[1]
            dic[host] = c
        return dic

    def get_top_requested_pages(self):
        '''
        Calculates the requested pages.
        
        Args:

        Returns:
            A collections object with all the requests made.
        '''
        return collections.Counter(self.data['REQUEST'])
    
    def get_top_unsuccessful_requested_pages(self):
        '''
        Calculates the requested pages that have failed.
        
        Args:

        Returns:
            A collections object with all the failed requests made.
        '''
        unsuccessful_pages = []
        for index, req in enumerate(self.data['REQUEST']):
            status_code = self.data['STATUS_CODE'][index]
            if status_code < 200 or status_code > 300:
                unsuccessful_pages.append(req)
        return collections.Counter(unsuccessful_pages)

    def get_top_hosts_that_request_pages(self):
        '''
        Calculates the hosts that have requested a page.
        
        Args:

        Returns:
            A collections object with all the hosts.
        '''
        return collections.Counter(self.data['HOST'])

    def get_top_hosts_that_requested_pages_and_the_top_pages(self):
        '''
        Calculates the top 10 hosts that requested a page and the pages they 
        requested.
        
        Args:

        Returns:
            A dictionary with all the top hosts and the pages they requested.
        '''
        dic = {}
        top_hosts = [h[0] for h in self.get_top_hosts_that_request_pages().most_common(10)]
        for t in top_hosts:
            dic[t] = []
            for index, h in enumerate(self.data['HOST']):
                if t == h:
                    dic[t].append(self.data['REQUEST'][index])
        return dic

    def calculate_percentage_of_successful_requests(self):
        '''
        Calculates the percentage of the requests that have been successfully 
        served.
        
        Args:

        Returns:
            A tuple containing the number of successful requests, the total 
            number of requests and the percentage of successful requests.
        '''
        
        successful_req = len(filter(lambda x:((x>=200)and(x<300)),
                                    self.data['STATUS_CODE']))
        total_req = len(self.data['STATUS_CODE'])
        percentage = (float(successful_req)/total_req)*100
        return (successful_req, total_req, percentage)

    def calculate_percentage_of_unsuccessful_requests(self):
        '''
        Calculates the percentage of the requests that have failed
        
        Args:

        Returns:
            A tuple containing the number of unsuccessful requests, the total 
            number of requests and the percentage of unsuccessful requests.
        '''
        success, total, s_percentage = self.calculate_percentage_of_successful_requests()
        unsuccessful_req = total-success
        # Alternative way for calculating unsuccessful requests.
        #unsuccessful_req = len(filter(lambda x:((x<200) or (x>300)),
        #                              self.data['STATUS_CODE']))
        u_percentage = 100-s_percentage
        return (unsuccessful_req, total, u_percentage)

    def _read_data(self):
        '''
        Helper function for extracting information from the HTTP access log file.
        
        Args:

        Returns:
            A generator object with information about the request for each line.
        '''
        print('Reading access log data...')
        with open(self.artifacts + self.log_file, 'r') as f:
            for num, line in enumerate(f, 1):
                try:
                    yield self._parse_log_line(line, num)
                except MalformedLogLine as e:
                    print(e)
                    continue
            

    def _parse_log_line(self, line, num):
        '''
        Helper function for parsing the line of the access log file. Raises 
        exception if malformed lines are found.
        
        Args:
            line: The line of the access log file.
            num: The number of the line.

        Returns:
            The maching groups of the line.
        '''
        log_pattern = r'([(\w\-\/\*\&\:\@\,\.)]+) - - \[(.*)\]\s*"(?:GET|POST|HEAD)\s*([(\w\-\/\.\_\:)]+)\s*.*"\s*(\d{3})\s*(\d+|-)\s*$'
        m = re.match(log_pattern, line)
        if m:
            return m.groups()
        else:
            msg = "Line {} is malformed.\n'{}' Skipping..."
            raise MalformedLogLine(msg.format(num, line))

    
        
    def _cleanup(self):
        '''
        Helper function for removing files from previous runs
        
        Args:

        Returns:
        
        '''
        try:
            os.mkdir(self.artifacts)
        except OSError as e:
            if 'File exists' in e:
                for item in os.listdir(self.artifacts):
                    os.remove(self.artifacts + item)

def parse_input():
    '''
    Parses the command line arguments.
    '''
    
    parser = ArgumentParser()
    parser.add_argument("-a", "--all",
                        help="Generates a complete report.",
                        action="store_true")
    parser.add_argument("-trp", "--top_requested_pages",
                        help="Generates a report only for the top 10 requested pages.",
                        action="store_true")
    parser.add_argument("-tur", "--top_unsuccessful_requests", 
                        help="Generates a report only for the top 10 unsuccessful requested pages.",
                        action="store_true")
    parser.add_argument("-th", "--top_hosts",
                        help="Generates a report only for the top 10 hosts that requested pages.",
                        action="store_true")
    parser.add_argument("-thp", "--top_hosts_pages",
                        help="Generates a report only for the top 10 hosts and the top 5 requested pages.",
                        action="store_true")
    parser.add_argument("-srp", "--successful_request_percentage",
                        help="Generates a report only for the percentage of successful requested pages.",
                        action="store_true")
    parser.add_argument("-urp", "--unsuccessful_request_percentage",
                        help="Generates a report only for the percentage of unsuccessful requested pages.",
                        action="store_true")
    return parser.parse_args()

def main():
    '''
    Main function that runs when module is executed.
    '''
    arguments = parse_input()
    if arguments.all and len(sys.argv) > 2:
        raise ArgumentsError('Argument -a/--all: not allowed with other arguments.')
    
    analyzer = Analyzer()
    analyzer.download_access_logs()
    analyzer.unzip_access_logs()
    analyzer.initiate_data()

    if arguments.all:
        arguments.top_requested_pages = True
        arguments.top_unsuccessful_requests = True
        arguments.top_hosts = True
        arguments.top_hosts_pages = True
        arguments.successful_request_percentage = True
        arguments.unsuccessful_request_percentage = True
    if arguments.top_requested_pages:
        arguments.top_requested_pages = analyzer.calculate_top_requested_pages()
    if arguments.top_unsuccessful_requests:
        arguments.top_unsuccessful_requests = analyzer.calculate_top_unsuccessful_requested_pages()
    if arguments.top_hosts:
        arguments.top_hosts = analyzer.calculate_top_hosts()
    if arguments.top_hosts_pages:
        arguments.top_hosts_pages = analyzer.calculate_top_hosts_and_top_requested_pages()
    if arguments.successful_request_percentage:
        arguments.successful_request_percentage = analyzer.calculate_percentage_of_successful_requests()
    if arguments.unsuccessful_request_percentage:
        arguments.unsuccessful_request_percentage = analyzer.calculate_percentage_of_unsuccessful_requests()

    analyzer.generate_report(vars(arguments))

if __name__ == "__main__":
    main()

