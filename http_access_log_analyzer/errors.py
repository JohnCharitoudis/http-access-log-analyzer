'''
Created on Nov 11, 2017

@author: johncharitoudis
'''

class DownloadError(Exception):
    pass

class UnzipError(Exception):
    pass

class MalformedLogLine(Exception):
    pass

class ArgumentsError(Exception):
    pass

class ReportError(Exception):
    pass